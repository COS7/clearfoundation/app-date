<?php

$lang['date_app_description'] = 'De tijdzone en synchronisatie tool.';
$lang['date_app_name'] = 'Datum en tijd';
$lang['date_app_tooltip'] = 'VoIP, authenticatie en andere toepassingen vereisen de juiste tijd instellingen, dus zorg ervoor dat uw klok en tijdzone correct zijn!';
$lang['date_automatic_synchronize'] = 'Automatisch synchroniseren';
$lang['date_date'] = 'Datum';
$lang['date_synchronize'] = 'Synchroniseren';
$lang['date_synchronize_now'] = 'Nu synchroniseer';
$lang['date_synchronize_wizard_tip'] = 'Met automatisch synchroniseren ingeschakeld, zal het systeem de klok van tijd tot tijd synchroniseren.';
$lang['date_synchronized'] = 'Gesynchroniseerd';
$lang['date_synchronizing'] = 'Synchroniseren';
$lang['date_time'] = 'Tijd';
$lang['date_time_server'] = 'Tijdserver';
$lang['date_time_server_is_invalid'] = 'De status van de tijdserver is ongeldig.';
$lang['date_time_synchronization_schedule_is_invalid'] = 'Het tijdsynchronisatie schema is ongeldig.';
$lang['date_time_zone'] = 'Tijdzone';
$lang['date_time_zone_is_invalid'] = 'De tijdzone is ongeldig.';
$lang['date_time_zone_wizard_help'] = 'Veel apps en diensten vereisen juiste tijdzone informatie en een nauwkeurige klok, dus het is belangrijk dat het correct is ingesteld.';
